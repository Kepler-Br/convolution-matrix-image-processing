# Convolution Matrix Image Processing

Program that allows you process BMP images using convolution matrix.  

**Available presets:**
- Gaussian blur.
- Box blur.
- 2 flavors of edge detection.
- Sharpen filter.
- Emboss filter.

**Features:**
- Absolutely not obsolete image format: BMP
- Multi-threading. You can create as many threads as you wish.
- Load any square custom matrix with odd width and height.

Finished version coming soon.

You should place compiled BMP lib in `BmpLib` folder
https://gitlab.com/Kepler-Br/bmp-library

You should place matrix algebra lib in `MatAlg` folder
https://gitlab.com/Kepler-Br/matrix-algebra
 
And most important: this program might not work under any OS other than GNU/Linux based OS.  
Sorry.

To compile project run this command:  
`qmake ./MatrixImageProcessing.pro; make -j`