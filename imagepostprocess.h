#ifndef IMAGEPOSTPROCESS_H
#define IMAGEPOSTPROCESS_H

#include <image.h>
#include "matrix.h"
#include "matrixmisc.h"


#include <queue>
#include <mutex>

enum ColorChannel {
    RED = 0, GREEN, BLUE
};

class ImagePostProcess
{
private:

    // Formula:
    //https://www.geeksforgeeks.org/gaussian-filter-generation-c/
    /*!
       \brief function that calculates matrix element for gaussian blur.
    */
    static double gaussKernelFormula(const double x, const double y);
    static MatAlg::Matrix<double> getImageKernel(const BmpLib::Image &img, const uint centerX,
                                                     const uint centerY, const uint kernelSize,
                                                     const ColorChannel channel);

    /*!
       \brief process every pixel in this thread function.
    */
    static void threadFunction(const BmpLib::Image *img, BmpLib::Image *result, const uint kernelSize,
                                 const MatAlg::Matrix<double> *filter, const double offset,
                                 const double divisor, std::queue<std::tuple<uint, uint> > *tasks,
                                 std::mutex *mut);

    /*!
       \brief function for clamping color between 255 and 0.
    */
    static double clampColor(const double &color);
    ImagePostProcess();
public:
    static MatAlg::Matrix<double> gaussBlurMatrix(const uint kernelSize);
    static MatAlg::Matrix<double> blurMatrix(const uint kernelSize);
    static MatAlg::Matrix<double> edgeDetectMatrix();
    static MatAlg::Matrix<double> edgeEnhanceMatrix();
    static MatAlg::Matrix<double> sharpenMatrix();
    static MatAlg::Matrix<double> embossMatrix();

    static BmpLib::Image run(MatAlg::Matrix<double> filter, const BmpLib::Image &img,
                                           const double offset, double divisor, const uint threads);

};

#endif // IMAGEPOSTPROCESS_H
