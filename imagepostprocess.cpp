#include "imagepostprocess.h"
#include "matrixmisc.h"


#include <thread>
#include <chrono>
#include <queue>
#include <cstdlib>

using namespace MatAlg;
using namespace BmpLib;

// Formula:
//https://www.geeksforgeeks.org/gaussian-filter-generation-c/
double ImagePostProcess::gaussKernelFormula(const double x, const double y)
{
    double sigma = 1.0;
    double firstPart = 1/(2*M_PI*pow(sigma, 2));
    double secondPart = exp(-(pow(x, 2) + pow(y, 2))/(2*pow(sigma, 2)));
    return firstPart*secondPart;
}

Matrix<double> ImagePostProcess::gaussBlurMatrix(const uint kernelSize)
{
    Matrix<double> mat(kernelSize, kernelSize, 0.0);
    uint center = kernelSize/2;
    for(uint x = 0; x < kernelSize; x++)
    {
        for(uint y = 0; y < kernelSize; y++)
        {
            double value = gaussKernelFormula(x-center, y-center);
            mat[x][y] = value;
        }
    }
    return mat;
}

MatAlg::Matrix<double> ImagePostProcess::blurMatrix(const uint kernelSize)
{
    return eye<double>(kernelSize);
}

Matrix<double> ImagePostProcess::edgeDetectMatrix()
{
    return {{0, 1, 0},
        {1, -4, 1},
        {0, 1, 0}};
}

Matrix<double> ImagePostProcess::edgeEnhanceMatrix()
{
    return {{0, 0, 0},
        {-1, 1, 0},
        {0, 0, 0}};
}

MatAlg::Matrix<double> ImagePostProcess::sharpenMatrix()
{
    return {{0, -1, 0},
        {-1, 5, -1},
        {0, -1, 0}};
}

MatAlg::Matrix<double> ImagePostProcess::embossMatrix()
{
    return {{-2, -1, 0},
        {-1, 1, 1},
        {0, 1, 2}};
}

Matrix<double> ImagePostProcess::getImageKernel(const Image &img, const uint centerX,
                                                const uint centerY, const uint kernelSize,
                                                const ColorChannel channel)
{
    Matrix<double> mat(kernelSize, kernelSize, 0.0);
    for(uint x = 0; x < kernelSize; x++)
    {
        for(uint y = 0; y < kernelSize; y++)
        {
            vec3b col = img.getPixel(centerX+x-kernelSize/2-1, centerY+y-kernelSize/2-1);
            if(channel == ColorChannel::RED)
                mat[x][y] = double(col.r);
            if(channel == ColorChannel::GREEN)
                mat[x][y] = double(col.g);
            if(channel == ColorChannel::BLUE)
                mat[x][y] = double(col.b);
        }
    }
    return mat;
}

ImagePostProcess::ImagePostProcess()
{

}

Image ImagePostProcess::run(MatAlg::Matrix<double> filter, const Image &img,
                                          const double offset, double divisor, const uint threadNum)
{
    if(filter.getColumnSize() != filter.getRowSize())
        throw std::logic_error("Filter matrix should be square.");
    if(filter.getColumnSize()%2 == 0)
        throw std::logic_error("Filter matrix should be odd in column and row sizes.");

    uint kernelSize = filter.getColumnSize();
    uint width = img.getWidth();
    uint height = img.getHeight();
    Image result(width, height);
    if(divisor == 0.0)
        divisor = elementSum(filter);

    std::queue<std::tuple<uint, uint>> tasks;
    std::vector<std::thread> threads;
    std::mutex mut;

    std::cout << "Creating tasks.\n";
    for(uint x = kernelSize; x < width-kernelSize; x++)
    {
        for(uint y = kernelSize; y < height-kernelSize; y++)
        {
            tasks.push(std::make_tuple(x, y));
        }
    }

    std::cout << "Creating threads.\n";
    for(uint i = 0; i < threadNum; i++)
    {
        threads.push_back(std::thread(threadFunction, &img, &result, kernelSize, &filter, offset, divisor, &tasks, &mut));
    }

    std::cout << "Waiting for threads.\n";
    for(uint i = 0; i < threadNum; i++)
    {
        threads[i].join();
    }

    return result;
}

void ImagePostProcess::threadFunction(const Image *img, Image *result, const uint kernelSize,
                                           const Matrix<double> *filter,
                                           const double offset, const double divisor,
                                           std::queue<std::tuple<uint, uint> > *tasks, std::mutex *mut)
{
    while(true)
    {
        // Get task from queue.
        mut->lock();
        if(tasks->empty())
        {
            mut->unlock();
            return;
        }

        uint x, y;
        std::tie(x, y) = tasks->front();
        tasks->pop();
        mut->unlock();

        // Process image.
        // Get image color channels loaded into matrix for futher process.
        Matrix<double> redChannel = getImageKernel(*img, x, y, kernelSize, ColorChannel::RED);
        double redResult = elementMul(*filter, redChannel);
        redResult = redResult/divisor+offset;

        Matrix<double> blueChannel = getImageKernel(*img, x, y, kernelSize, ColorChannel::BLUE);
        double blueResult = elementMul(*filter, blueChannel);
        blueResult = blueResult/divisor+offset;

        Matrix<double> greenChannel = getImageKernel(*img, x, y, kernelSize, ColorChannel::GREEN);
        double greenResult = elementMul(*filter, greenChannel);
        greenResult = greenResult/divisor+offset;

        // Clamp result, so u_char will not overflow.
        redResult = clampColor(redResult);
        greenResult = clampColor(greenResult);
        blueResult = clampColor(blueResult);

        vec3b color;
        color.r = u_char(redResult);
        color.g = u_char(greenResult);
        color.b = u_char(blueResult);
        result->setPixel(x, y, color);
    }
}

double ImagePostProcess::clampColor(const double &color)
{
    if(color >= 0 && color <= 255)
        return color;
    if(color < 0)
        return 0;
    if(color > 255)
        return 255;
    // -Wreturn-type
    return color;
}
