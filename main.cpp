#include <iostream>
#include <bmploader.h>
#include <bmpwriter.h>
#include <image.h>
#include "imagepostprocess.h"

using namespace MatAlg;
using namespace BmpLib;

using namespace std;

int main()
{
    BmpLoader loader;
    Image skeleton = loader.load("blender.bmp");
    Matrix<double> filter = {{-1, -1, -1}, {2, 2, 2}, {-1, -1, -1}};
//    Matrix<double> filter = ImagePostProcess::edgeDetectMatrix();
    Image bluredSkeleton = ImagePostProcess::run(filter, skeleton, 0.0, 0.0, 16);
    BmpWriter writer;
    writer.write(bluredSkeleton, "newSkeletonNew.bmp");
    cout << "Done!" << endl;
    return 0;
}
