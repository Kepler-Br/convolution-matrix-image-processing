TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        imagepostprocess.cpp

HEADERS += \
        imagepostprocess.h 

INCLUDEPATH += ./MatAlg

LIBS += -lpthread

unix:!macx: LIBS += -L$$PWD/BmpLib/ -llibbmp

INCLUDEPATH += $$PWD/BmpLib
DEPENDPATH += $$PWD/BmpLib

unix:!macx: PRE_TARGETDEPS += $$PWD/BmpLib/libbmp.a
